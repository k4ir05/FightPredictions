class << self

  def fighters
    [
      {
        first_name: 'Fabricio',
        last_name: 'Werdum',
        alias: '',
        sport_kind: 1
      },
      {
        first_name: 'Cain',
        last_name: 'Velasquez',
        alias: '',
        sport_kind: 1
      },
      {
        first_name: 'Daniel',
        last_name: 'Cormier',
        alias: '',
        sport_kind: 1
      },
      {
        first_name: 'Anthony',
        last_name: 'Johnson',
        alias: '',
        sport_kind: 1
      },
      {
        first_name: 'Jacare',
        last_name: 'Souza',
        alias: '',
        sport_kind: 1
      },
      {
        first_name: 'Gegard',
        last_name: 'Mousasi',
        alias: '',
        sport_kind: 1
      },
      {
        first_name: 'Tyron',
        last_name: 'Woodley',
        alias: '',
        sport_kind: 1
      },
      {
        first_name: 'Stephen',
        last_name: 'Thompson',
        alias: '',
        sport_kind: 1
      },
      {
        first_name: 'Eddie',
        last_name: 'Alvarez',
        alias: '',
        sport_kind: 1
      },
      {
        first_name: 'Rafael',
        last_name: 'Dos Anjos',
        alias: '',
        sport_kind: 1
      },
      {
        first_name: 'Jose',
        last_name: 'Aldo',
        alias: '',
        sport_kind: 1
      },
      {
        first_name: 'Max',
        last_name: 'Holloway',
        alias: '',
        sport_kind: 1
      }
    ]
  end
end

fighters.each do |attributes|
  Fighter.where(attributes.slice(:first_name, :last_name)).first_or_create!(attributes)
end
