class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.references :author, null: false, foreign_key: {to_table: :users}, index: true
      t.string :name, null: false, index: true
      t.datetime :start_time, null: false, index: true
      t.text :description
      t.integer :kind, null: false, default: 0, index: true
      t.integer :status, null: false, default: 0, index: true

      t.timestamps
    end
  end
end
