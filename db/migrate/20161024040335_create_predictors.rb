class CreatePredictors < ActiveRecord::Migration[5.0]
  def change
    create_table :predictors do |t|
      t.string :name, null: false, index: {unique: true}
      t.references :account, index: true, foreign_key: {to_table: :users}

      t.timestamps
    end
  end
end
