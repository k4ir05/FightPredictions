class CreatePredictions < ActiveRecord::Migration[5.0]
  def change
    create_table :predictions do |t|
      t.references :predictor, foreign_key: true
      t.references :fight, foreign_key: true
      t.references :winner, foreign_key: {to_table: :fighters}
      t.integer :way
      t.integer :round

      t.timestamps
    end
  end
end
