class CreateFights < ActiveRecord::Migration[5.0]
  def change
    create_table :fights do |t|
      t.references :event, foreign_key: true, index: true
      t.references :fighter1, foreign_key: {to_table: :fighters}, index: true
      t.references :fighter2, foreign_key: {to_table: :fighters}, index: true

      t.timestamps
    end
  end
end
