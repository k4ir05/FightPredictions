admin = User.where(username: 'admin').first_or_create! role: 0,
                                                       email: Rails.application.secrets.admin_email,
                                                       password: Rails.application.secrets.admin_password

Predictor.where(account: admin).first_or_create! name: 'Admin'