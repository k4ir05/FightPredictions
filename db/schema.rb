# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161024040400) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "events", force: :cascade do |t|
    t.integer  "author_id",               null: false
    t.string   "name",                    null: false
    t.datetime "start_time",              null: false
    t.text     "description"
    t.integer  "kind",        default: 0, null: false
    t.integer  "status",      default: 0, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["author_id"], name: "index_events_on_author_id", using: :btree
    t.index ["kind"], name: "index_events_on_kind", using: :btree
    t.index ["name"], name: "index_events_on_name", using: :btree
    t.index ["start_time"], name: "index_events_on_start_time", using: :btree
    t.index ["status"], name: "index_events_on_status", using: :btree
  end

  create_table "fighters", force: :cascade do |t|
    t.string   "first_name",             null: false
    t.string   "last_name",              null: false
    t.string   "alias"
    t.integer  "sport_kind", default: 0, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["alias"], name: "index_fighters_on_alias", using: :btree
    t.index ["first_name"], name: "index_fighters_on_first_name", using: :btree
    t.index ["last_name"], name: "index_fighters_on_last_name", using: :btree
    t.index ["sport_kind"], name: "index_fighters_on_sport_kind", using: :btree
  end

  create_table "fights", force: :cascade do |t|
    t.integer  "event_id"
    t.integer  "fighter1_id"
    t.integer  "fighter2_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["event_id"], name: "index_fights_on_event_id", using: :btree
    t.index ["fighter1_id"], name: "index_fights_on_fighter1_id", using: :btree
    t.index ["fighter2_id"], name: "index_fights_on_fighter2_id", using: :btree
  end

  create_table "predictions", force: :cascade do |t|
    t.integer  "predictor_id"
    t.integer  "fight_id"
    t.integer  "winner_id"
    t.integer  "way"
    t.integer  "round"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["fight_id"], name: "index_predictions_on_fight_id", using: :btree
    t.index ["predictor_id"], name: "index_predictions_on_predictor_id", using: :btree
    t.index ["winner_id"], name: "index_predictions_on_winner_id", using: :btree
  end

  create_table "predictors", force: :cascade do |t|
    t.string   "name",       null: false
    t.integer  "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_predictors_on_account_id", using: :btree
    t.index ["name"], name: "index_predictors_on_name", unique: true, using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.integer  "role",                   default: 1,  null: false
    t.string   "username",               default: "", null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "events", "users", column: "author_id"
  add_foreign_key "fights", "events"
  add_foreign_key "fights", "fighters", column: "fighter1_id"
  add_foreign_key "fights", "fighters", column: "fighter2_id"
  add_foreign_key "predictions", "fighters", column: "winner_id"
  add_foreign_key "predictions", "fights"
  add_foreign_key "predictions", "predictors"
  add_foreign_key "predictors", "users", column: "account_id"
end
