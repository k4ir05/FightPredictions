Rails.application.routes.draw do
  devise_for :users
  root 'pages#home'

  resources :predictors
  resources :fighters
  resources :events, shallow: true do
    resources :fights
    resources :predictions, only: :index
  end
  resources :fights, shallow: true do
    resources :predictions, only: %i[create update destroy]
  end
end
