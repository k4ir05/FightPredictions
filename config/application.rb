require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module FightPredictions
  class Application < Rails::Application
    config.time_zone = 'Moscow'

    config.i18n.default_locale = :ru
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}').to_s]

    config.logger = ActiveSupport::Logger.new config.paths['log'].first, 5, 50.megabytes
  end
end
