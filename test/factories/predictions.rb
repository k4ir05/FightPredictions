FactoryGirl.define do
  factory :prediction do
    predictor nil
    fight nil
    winner nil
    way 1
    round 1
  end
end
