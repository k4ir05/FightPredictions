require "test_helper"

class PredictorTest < ActiveSupport::TestCase
  def predictor
    @predictor ||= Predictor.new
  end

  def test_valid
    assert predictor.valid?
  end
end
