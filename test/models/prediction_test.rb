require "test_helper"

class PredictionTest < ActiveSupport::TestCase
  def prediction
    @prediction ||= Prediction.new
  end

  def test_valid
    assert prediction.valid?
  end
end
