require "test_helper"

class FighterTest < ActiveSupport::TestCase
  def fighter
    @fighter ||= Fighter.new
  end

  def test_valid
    assert fighter.valid?
  end
end
