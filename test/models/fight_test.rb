require "test_helper"

class FightTest < ActiveSupport::TestCase
  def fight
    @fight ||= Fight.new
  end

  def test_valid
    assert fight.valid?
  end
end
