window.initializeAutocomplete = ->
  $('.autocomplete-input').each ->
    $input = $(this)
    $field = $('.text-field', $input)
    $field.autocomplete
      source: $field.data('source')
      minLength: $field.data('min-length')
      focus: (event, ui)->
        $(this).val(ui.item.name)
        false
      select: (event, ui)->
        $(this).val(ui.item.name)
        $('.id-field', $input).val(ui.item.id)
        false
    $field.autocomplete('instance')._renderItem = (ul, item)->
      if item.description is undefined
        $('<li>').append("<a>#{item.name}</a>").appendTo(ul)
      else
        $('<li>').append("<a><strong>#{item.name}</strong><br>#{item.description}</a>").appendTo(ul)
#        $('<li>').append("<a>#{item.name}<br><i>#{item.description}</i></a>").appendTo(ul)
    $field.autocomplete('instance')._resizeMenu = ->
      this.menu.element.outerWidth($field.outerWidth())
    $('.button', $input).click (event)->
      $field.autocomplete 'search', ''
      $field.focus()
    $field.change ->
      if $field.val() == ''
        $('.id-field', $input).val('')