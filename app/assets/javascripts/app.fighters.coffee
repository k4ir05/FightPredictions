App.Fighters =

  initAutocomplete: ->
    $('.fighter-search').typeahead
      ajax: '/fighters',
      onSelect: (item)->
        $input = $('.fighter-search.active')
        $input.siblings('.fighter-id').val(item.value)
        $input.siblings('.edit-link').attr('href', "/fighters/#{item.value}/edit").attr('data-remote', true)

  modalSelect: (fighter)->
    fighter = JSON.parse(fighter)
    $input = $('.fighter.input-group.active')
    $input.find('.fighter-id').val(fighter.id)
    $input.find('.fighter-search').val(fighter.name)
    $input.find('[data-behavior~=edit-fighter]').attr('href', "/fighters/#{fighter.id}/edit").attr('data-remote', true)
    App.hideModal()

$(document).on 'click', '[data-behavior~=new-fighter], [data-behavior~=edit-fighter]', (event)->
  event.preventDefault()
  $('.fighter.input-group.active').removeClass('active')
  $(this).closest('.fighter.input-group').addClass('active')

$(document).on 'focus', '.fighter-search', ->
  $('.fighter-search.active').removeClass('active')
  $(this).addClass('active')