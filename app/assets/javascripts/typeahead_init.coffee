Typeahead =
  init: ->
    $('input.typeahead').typeahead()

$(document).on 'turbolinks:load', ->
  Typeahead.init()