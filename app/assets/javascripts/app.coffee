window.App ||= {}

App.init = ->
  $("a[href^='http']").attr('target', '_blank')

App.showModal = (content, size=null)->
  if $('#modal').length == 0
    $("body").prepend(content)
  else
    $('#modal').replaceWith(content)
  $('#modal').find('.modal-dialog').addClass("modal-#{size}") if size
  $("#modal:hidden").modal('show');

App.hideModal = ->
  $('#modal').modal('hide')