class Fighter < ApplicationRecord
  scope :search, ->(query='') do
    where 'first_name ILIKE :q OR last_name ILIKE :q OR alias ILIKE :q', q: "%#{query}%"
  end

  enum sport_kind: %i[boxing mma]

  def name
    [first_name, last_name].compact.join(' ')
  end

  def presentation
    [first_name, self.alias, last_name].compact.join(' ')
  end
end