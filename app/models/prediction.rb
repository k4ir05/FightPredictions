class Prediction < ApplicationRecord
  belongs_to :predictor
  belongs_to :fight
  belongs_to :winner, class_name: 'Fighter'

  delegate :fighter1, :fighter2, to: :fight, allow_nil: true
end
