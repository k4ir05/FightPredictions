class Fight < ApplicationRecord
  belongs_to :event
  belongs_to :fighter1, class_name: 'Fighter'
  belongs_to :fighter2, class_name: 'Fighter'
  has_many :predictions

  delegate :upcoming?, to: :event
end
