class Predictor < ApplicationRecord
  belongs_to :account, class_name: 'User', optional: true
  has_many :predictions

  scope :search, ->(query='') { where 'name ILIKE :q', q: "%#{query}%" }
end
