class Event < ApplicationRecord
  belongs_to :author, class_name: 'User'
  has_many :fights
  has_many :predictions, through: :fights
  enum kind: %i[boxing mma]
  enum status: %i[upcoming active finished]
end