class FighterInput < SimpleForm::Inputs::StringInput

  def input(wrapper_options = nil)
    template.content_tag(:div, class: 'fighter input-group', id: "#{attribute_name}-input") do
      search_field +
      @builder.hidden_field("#{attribute_name}_id", class: 'fighter-id') +
      template.content_tag(:span, link_to_edit, class: 'input-group-btn') +
      template.content_tag(:span, link_to_new, class: 'input-group-btn')
    end
  end

  private

  def name
    @builder.object.send(attribute_name)&.name
  end

  def search_field
    template.text_field_tag "#{attribute_name}_search", name,
                            id: "#{attribute_name}-search",
                            class: 'fighter-search search-input form-control',
                            # placeholder: I18n.t('helpers.placeholders.fighter_input'),
                            autofocus: true, autocomplete: 'off'
  end

  def link_to_new
    template.link_to(template.icon(:plus), template.new_fighter_path, class: 'btn btn-default', remote: true,
                     data: {behavior: 'new-fighter'})
  end

  def link_to_edit
    template.link_to(template.icon(:edit), edit_path, class: 'edit-link btn btn-default',
                     remote: @builder.object.send(attribute_name).present?, data: {behavior: 'edit-fighter'})
  end

  def edit_path
    @builder.object.send(attribute_name).present? ? template.edit_fighter_path(@builder.object.send(attribute_name)) : '#'
  end

end
