class Event < ApplicationRecord
  class Create < Operation::Persist
    model!
    policy!
    contract!

    private

    def setup_model!(params)
      model.author = params[:current_user]
    end
  end
end