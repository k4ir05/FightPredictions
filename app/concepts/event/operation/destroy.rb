class Event < ApplicationRecord
  class Destroy < Operation::Destroy
    model!
    policy!
  end
end