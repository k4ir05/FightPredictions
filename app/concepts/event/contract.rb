class Event < ApplicationRecord
  class Contract < Contract::Base
    feature Reform::Form::ActiveModel::FormBuilderMethods
    feature Reform::Form::MultiParameterAttributes

    model Event
    properties :name, :description, :kind
    property :start_time, multi_params: true
    validates :name, :kind, :start_time, presence: true

    # collection :fights do
    #   properties :fighter1_id, :fighter2_id, validates: {presence: true}
    # end
  end
end