class Event < ApplicationRecord
  module Upcoming
    module Cell
      class Index < Event::Cell::Index

        def show
          render :index
        end

        def title
          t 'events.index.upcoming.title'
        end

        def events_list
          model.map do |event|
            concept('event/cell/upcoming/item', event).()
          end.join
        end
      end
    end
  end
end
