class Event < ApplicationRecord
  module Upcoming
    module Cell
      class Preview < Event::Cell::Preview
        def name_link
          link_to name, event_path(model)
        end
      end
    end
  end
end
