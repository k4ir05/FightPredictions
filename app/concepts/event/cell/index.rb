class Event < ApplicationRecord
  module Cell
    class Index < Cell::Index

      def title
        t 'events.index.title'
      end

      def new_link
        link_to t('events.new.title'), new_event_path
      end

      def attribute_name(attribute)
        Event.human_attribute_name attribute
      end

      def events_list
        cell(Event::Cell::Preview, collection: collection).call
      end
    end
  end
end