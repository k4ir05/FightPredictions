class Event < ApplicationRecord
  module Cell
    class Preview < Cell::Model
      property :name

      def name_link
        link_to name, model
      end

      def date
        I18n.l model.start_time, format: :full_date
      end

      def kind
        Event.human_attribute_name "kind.#{model.kind}"
      end

      def edit_link
        link_to icon(:edit), edit_event_path(model), class: 'btn btn-default'
      end

      def delete_link
        link_to icon(:trash), model, data: {confirm: t('helpers.confirmation')}, method: :delete, class: 'btn btn-danger'
      end
    end
  end
end
