class Event < ApplicationRecord
  module Cell
    class Show < Preview
      property :description

      def time
        I18n.l model.start_time, format: :full
      end

      def edit_link
        link_to t('helpers.links.edit'), edit_event_path(model), class: 'btn btn-default'
      end

      def description_panel
        unless description.blank?
          content_tag :div, class: 'panel panel-default' do
            content_tag :div, description, class: 'panel-body'
          end
        end
      end
    end
  end
end