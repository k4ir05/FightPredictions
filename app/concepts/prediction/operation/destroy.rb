class Prediction < ApplicationRecord
  class Destroy < Operation::Destroy
    model!
    policy!
  end
end