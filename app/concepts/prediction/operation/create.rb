class Prediction < ApplicationRecord
  class Create < Operation::Persist
    model!
    policy!
    contract!
  end
end