class Prediction < ApplicationRecord
  module Cell
    class Index < Cell::Index

      def title
        t 'predictions.index.title'
      end

      def predictions_list
        cell(Preview, collection: collection).call
      end
    end
  end
end