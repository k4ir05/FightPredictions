class Prediction < ApplicationRecord
  module Cell
    class Item < Cell::Model
      property :id, :fighter1, :fighter2, :winner, :way, :round

      def fighter1_presentation
        fighter_presentation fighter1
      end

      def fighter2_presentation
        fighter_presentation fighter2
      end

      def result
        [way, round].compact.join('-')
      end

      def delete_link
        link_to icon(:trash), model, data: {confirm: t('helpers.confirmation')}, method: :delete, remote: true,
                class: 'btn btn-danger btn-xs'
      end

      private

      def fighter_presentation(fighter)
        _class = 'winner' if fighter == winner
        link_to fighter.presentation, fighter, class: _class
      end
    end
  end
end