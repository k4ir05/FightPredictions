class Prediction < ApplicationRecord
  class Contract < Contract::Base
    model Prediction
    properties :fight_id, :winner_id, :way, :round
    validates :fight_id, :winner_id, :way, presence: true
  end
end