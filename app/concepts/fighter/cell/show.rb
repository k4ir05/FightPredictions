class Fighter < ApplicationRecord
  module Cell
    class Show < Cell::Model

      protected

      delegates :model, :first_name, :last_name, :alias

      def sport_kind
        Fighter.human_attribute_name "sport_kind.#{model.sport_kind}"
      end

      def edit_link
        link_to icon(:edit), edit_fighter_path(model), class: 'btn btn-default'
      end

      def delete_link
        link_to icon(:trash), model, data: {confirm: t('helpers.confirmation')}, method: :delete, class: 'btn btn-danger'
      end
    end
  end
end