class Fighter < ApplicationRecord
  module Cell
    class Preview < Show

      protected

      def first_name
        link_to super, model
      end

      def last_name
        link_to super, model
      end

      def alias
        link_to super, model
      end
    end
  end
end