class Fighter < ApplicationRecord
  module Cell
    class Index < Cell::Index

      def title
        t 'fighters.index.title'
      end

      def new_link
        link_to t('fighters.new.title'), new_fighter_path
      end

      def attribute_name(attribute)
        Fighter.human_attribute_name attribute
      end

      def fighters
        model
      end
    end
  end
end