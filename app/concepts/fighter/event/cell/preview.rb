class Fighter < ApplicationRecord
  module Event
    module Cell
      class Preview < Fighter::Cell::Preview

        def presentation_link
          link_to model.presentation, model
        end
      end
    end
  end
end