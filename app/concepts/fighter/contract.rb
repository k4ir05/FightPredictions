class Fighter < ApplicationRecord
  class Contract < Contract::Base
    model Fighter
    properties :first_name, :last_name, :alias, :sport_kind
    validates :first_name, :last_name, presence: true
  end
end