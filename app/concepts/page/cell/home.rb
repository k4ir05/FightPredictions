module Page
  module Cell
    class Home < Cell::Base

      def title
        t 'application_name'
      end
    end
  end
end