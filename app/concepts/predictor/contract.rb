class Predictor < ApplicationRecord
  class Contract < Contract::Base
    model Predictor
    property :name
    validates :name, presence: true#, unique: true
  end
end