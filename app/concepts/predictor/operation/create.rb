class Predictor < ApplicationRecord
  class Create < Operation::Persist
    model!
    policy!
    contract!
  end
end