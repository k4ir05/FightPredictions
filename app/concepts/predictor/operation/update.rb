class Predictor < ApplicationRecord
  class Update < Operation::Persist
    model!
    policy!
    contract!
  end
end