class Fight < ApplicationRecord
  class Contract < Contract::Base
    model Fight
    properties :fighter1_id, :fighter2_id, validates: {presence: true}
    properties :fighter1, :fighter2, :event, writeable: false

  end
end