class Fight < ApplicationRecord
  module Event
    module Cell
      class Preview < Cell::Model
        private

        property :id

        def fighter1
          fighter_item model.fighter1
        end

        def fighter2
          fighter_item model.fighter2
        end

        def delete_link
          link_to icon(:trash), model, data: {confirm: t('helpers.confirmation')}, method: :delete, remote: true,
                  class: 'btn btn-danger btn-xs'
        end

        def fighter_item(fighter)
          concept('fighter/cell/event/item', fighter).()
        end
      end
    end
  end
end