class Fight < ApplicationRecord
  module Event
    module Cell
      class Index < Cell::Index

        def title
          t 'fights.index.title'
        end

        def fights_list
          cell(Fight::Event::Cell::Preview, collection: collection).call
        end

        def add_fight_link
          link_to t('fights.new.title'), new_event_fight_path(event), class: 'btn btn-default btn-sm', remote: true
        end

        private

        def event
          options[:event]
        end
      end
    end
  end
end