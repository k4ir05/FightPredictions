class Fight < ApplicationRecord
  class Update < FightPredictions::Operation
    model Fight, :update
    policy Policy, :update?
    contract Contract
  end
end