class Fight < ApplicationRecord
  class Create < Operation::Persist
    model!
    policy!
    contract!

    private

    def setup_model!(params)
      event = Event.find params[:event_id]
      model.event = event
    end
  end
end