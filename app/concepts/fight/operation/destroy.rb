class Fight < ApplicationRecord
  class Destroy < FightPredictions::Operation::Destroy
    model Fight, :find
    policy Policy, :destroy?
  end
end