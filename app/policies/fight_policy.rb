class FightPolicy < ApplicationPolicy

  def index?
    true
  end

  def show?
    true
  end

  def create?
    true
  end

  def update?
    true
  end

  def predict?
    record.upcoming? && user.present?
  end

  def destroy?
    true
  end
end
