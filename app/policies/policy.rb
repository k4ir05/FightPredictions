class EventPolicy < ApplicationPolicy

  def index?
    true
  end

  def show?
    true
  end

  def create?
    true
  end

  def update?
    is_owned?
  end

  def predict?
    record.upcoming? and user.present?
  end

  def destroy?
    is_owned?
  end

  private

  # def event_upcoming?
  #   record.upcoming?
  # end

  def is_owned?
    record.author == user
  end
end
