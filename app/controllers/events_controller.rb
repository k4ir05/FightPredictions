class EventsController < ApplicationController

  def index
    events = Event.all
    respond_to do |format|
      format.html { render_cell Event::Cell::Index, events }
    end
  end

  def show
    event = Event.includes(:fights).find(params[:id])
    authorize event
    respond_to do |format|
      format.html { render_cell Event::Cell::Show, event }
    end
  end

  def new
    form Event::Create
    respond_to do |format|
      format.html { render_form }
    end
  end

  def create
    respond_to do |format|
      run Event::Create do |op|
        format.html { return redirect_to op.model }
      end
      format.html { render :new }
    end
  end

  def edit
    form Event::Update
    respond_to do |format|
      format.html
    end
  end

  def update
    respond_to do |format|
      run Event::Update do |op|
        format.html { return redirect_to op.model }
      end
      format.html { render :edit }
    end
  end

  def destroy
    respond_to do |format|
      run Event::Destroy do |_|
      end
      format.html { redirect_to events_path }
    end
  end

  private

  def render_form
    render_cell Event::Cell::Form, contract: @operation.contract
  end
end