class ApplicationController < ActionController::Base
  include Trailblazer::Operation::Controller
  include Pundit
  protect_from_forgery with: :exception
  rescue_from Trailblazer::NotAuthorizedError, with: :not_authorized

  private

  def not_authorized
    # policy_name = exception.policy.class.to_s.underscore
    # message = t "#{policy_name}.#{exception.query}", scope: 'pundit', default: :default
    message = t 'policy.errors.not_authorized'
    if request.xhr?
      render_error message
    else
      flash[:alert] = message
      redirect_to request.referrer || root_path
    end
  end

  def render_error(error)
    render_notification error, 'alert'
  end

  def render_notification(text, type = 'info')
    render js: "App.Notification.show('#{text}', '#{type}');"
  end

  def params!(params)
    params.merge current_user: current_user
  end

  def render_cell(cell_class, model: nil, **options)
    model ||= @operation&.model
    render html: cell(cell_class, model, options).call, layout: layout_name
  end

  def layout_name
    'application'
  end
end
