class PagesController < ApplicationController

  def home
    events = Event.upcoming
    respond_to do |format|
      format.html { render_cell Page::Cell::Home, events }
    end
  end
end