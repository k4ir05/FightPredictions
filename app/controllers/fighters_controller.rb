class FightersController < ApplicationController

  def index
    @fighters = Fighter.search(params[:query])
    respond_to do |format|
      format.html
      format.json
    end
  end

  def show
    @fighter = Fighter.find params[:id]
    respond_to do |format|
      format.html
    end
  end

  def new
    form Fighter::Create
    respond_to do |format|
      format.html
      format.js { render :show_form }
    end
  end

  def create
    respond_to do |format|
      run Fighter::Create do |op|
        format.html { return redirect_to op.model }
        format.js { return render :save }
      end
      format.html { render :new }
      format.js { render :new }
    end
  end

  def edit
    form Fighter::Update
    respond_to do |format|
      format.html
      format.js { render :show_form }
    end
  end

  def update
    respond_to do |format|
      run Fighter::Update do |op|
        format.html { return redirect_to op.model }
        format.js { return render :save }
      end
      format.html { render :edit }
      format.js { render :edit }
    end
  end

  def destroy
    respond_to do |format|
      run Fighter::Destroy do |_|
      end
      format.html { redirect_to fighters_path }
    end
  end
end