class PredictorsController < ApplicationController

  def index
    @predictors = Predictor.search(params[:query])
    respond_to do |format|
      format.html
    end
  end

  def show
    @predictor = Predictor.find params[:id]
    respond_to do |format|
      format.html
    end
  end

  def new
    form Predictor::Create
    respond_to do |format|
      format.html
    end
  end

  def create
    respond_to do |format|
      run Predictor::Create do |op|
        format.html { return redirect_to op.model }
      end
      format.html { render :new }
    end
  end

  def edit
    form Predictor::Update
    respond_to do |format|
      format.html
    end
  end

  def update
    respond_to do |format|
      run Predictor::Update do |op|
        format.html { return redirect_to op.model }
      end
      format.html { render :edit }
    end
  end

  def destroy
    respond_to do |format|
      run Predictor::Destroy do |_|
      end
      format.html { redirect_to predictors_url }
    end
  end
end