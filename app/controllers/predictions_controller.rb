class PredictionsController < ApplicationController

  def index
    @event = Event.find params[:event_id]
    @predictions = @event.predictions.where(predictor: predictor)
    respond_to do |format|
      format.html
    end
  end

  def show
    @prediction = Prediction.find params[:id]
    respond_to do |format|
      format.html
    end
  end

  def new
    form Prediction::Create
    respond_to do |format|
      format.html
    end
  end

  def create
    respond_to do |format|
      run Prediction::Create do |op|
        format.html { return redirect_to op.model }
      end
      format.html { render :new }
    end
  end

  def edit
    form Prediction::Update
    respond_to do |format|
      format.html
    end
  end

  def update
    respond_to do |format|
      run Prediction::Update do |op|
        format.html { return redirect_to op.model }
      end
      format.html { render :edit }
    end
  end

  def destroy
    respond_to do |format|
      run Predictor::Destroy do |_|
      end
      format.html { redirect_to predictions_url }
    end
  end

  private

  def predictor
    @predictor ||= Predictor.find_by account: current_user
  end
end