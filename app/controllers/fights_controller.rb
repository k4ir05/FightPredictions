class FightsController < ApplicationController

  def index
    @fights = Fight.all
  end

  def show
    @fight = Fight.find params[:id]
    respond_to do |format|
      format.html
    end
  end

  def new
    form Fight::Create
    respond_to do |format|
      format.js
    end
  end

  def create
    respond_to do |format|
      run Fight::Create do |op|
        format.html { return redirect_to op.model }
        format.js { return }
      end
      format.html { render :new }
      format.js { render :new }
    end
  end

  def edit
    form Fight::Update
    respond_to do |format|
      format.html
    end
  end

  def update
    respond_to do |format|
      run Fight::Update do |op|
        format.html { return redirect_to op.model }
      end
      format.html { render :edit }
    end
  end

  def destroy
    respond_to do |format|
      run Fight::Destroy do |_|
      end
      format.js { redirect_to fights_path }
    end
  end
end