module ApplicationHelper

  def title(addition = nil)
    base_title = t 'application_name'
    addition.present? ? "#{base_title} - #{addition}" : base_title
  end
end
