module ModalHelper

  def close_modal_button
    content_tag(:button, type: 'button', class: 'close', data: {dismiss: 'modal'}) do
      content_tag(:span, icon(:times), aria: {hidden: true})
    end
  end
end