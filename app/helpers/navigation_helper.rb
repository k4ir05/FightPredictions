module NavigationHelper

  def navigation_item_tag(name, url, options = {})
    html_class = 'nav-item nav-link'
    url_controller = url.split('/')[1]
    html_class << ' active' if controller_name == url_controller
    options[:class] = html_class
    link_to name, url, options
  end
end