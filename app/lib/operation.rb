module Operation

  class Base < Trailblazer::Operation
    include Trailblazer::Operation::Policy
    include Model

    def model_name
      model.model_name.name
    end

    class << self

      def model!(model_class = nil)
        model_class ||= parents[-2]
        action = name.demodulize == 'Create' ? :create : :find
        model model_class, action
      end

      def policy!(policy_class = nil)
        # policy_class ||= "#{parent_name}Policy".safe_constantize
        policy_class ||= const_get "#{parent_name}Policy"
        action = "#{name.demodulize.underscore}?"
        policy policy_class, action
      end

      def contract!
        contract_class ||= begin
          base_name = "#{parent_name}::Contract"
          action_name = "#{base_name}::#{name.demodulize}"
          # action_name.safe_constantize || base_name.safe_constantize
          const_get(action_name || base_name)
        end
        contract contract_class
      end
    end
  end

  class Index < Base
    include Collection
  end

  class Persist < Base

    def process(params)
      model_params = get_model_params params
      validate model_params do
        contract.save
        ActiveRecord::Base.transaction do
          begin
            destroy_nested_resources!(model_params)
            contract.save
          rescue ActiveRecord::Rollback
            failure!
          end
        end
      end
    end

    private

    def get_model_params(params)
      model_params = params[model.model_name.param_key.to_sym]
      model_params.respond_to?(:to_unsafe_h) ? model_params.to_unsafe_h : model_params
    end

    def destroy_nested_resources!(params)
      nested_params = params.keep_if { |_, value| value.is_a?(Array) && value.first.is_a?(Hash) }
      nested_params.each do |resources_name, resources_params|
        ids = resources_params.collect do |resource_params|
          resource_params['id'] if resource_params['_destroy'].eql?('1')
        end.compact

        model.send(resources_name).where(id: ids).destroy_all
        contract.send(resources_name).reject! { |item| ids.include? item.id }
      end
    end
  end

  class Destroy < Base

    def process(_)
      model.destroy
    end
  end

  # class Restore < Base
  #
  #   def process(_)
  #     model.restore!
  #   end
  #
  #   private
  #
  #   def model!(params)
  #     model_class.trashed.find params[:id]
  #   end
  # end

  # class Trash < Base
  #
  #   def process(_)
  #     model.trash!
  #   end
  # end
end