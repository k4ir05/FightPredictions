module Cell
  class Base < Trailblazer::Cell
    include ActionView::Helpers::TranslationHelper
    include Cell::Translation
    include ActionView::Helpers::UrlHelper
    include FontAwesome::Sass::Rails::ViewHelpers
    # include ActionView::Helpers::CaptureHelper

    private

    delegate :current_user, :view_context, to: :controller

    def title
      t '.title'
    end

    def index_link
      link_to icon('chevron-left'), index_path, class: 'btn btn-link'
    end

    def index_path
      root_path
    end
  end

  class Model < Base
    private

    property :model_name

    def t_attribute(name)
      model_class.human_attribute_name name
    end

    def new_link(url = nil, **options)
      url ||= url_for(action: :new, controller: model_name.route_key)
      options[:class] = 'btn btn-outline-success'
      link_to icon('plus'), url, options
    end

    def edit_link(**options)
      options[:class] = 'btn btn-secondary'
      link_to icon('edit'), [:edit, model], options
    end

    def delete_link(**options)
      options[:class] = 'btn btn-danger'
      options[:method] = :delete
      link_to icon('trash'), model, options
    end

    def model_class
      model_name.name.constantize
    end
  end

  class Form < Model
    include SimpleForm::ActionViewExtensions::FormHelper
    # include Formular::Helper
    # include Formular::RailsHelper

    private

    property :persisted?

    def contract
      options.fetch :contract, model
    end

    def title
      action_name = model.persisted? ? 'edit' : 'new'
      t ".title.#{action_name}"
    end

    def form(**options, &block)
      options[:wrapper] = :horizontal_form
      simple_form_for contract, options, &block
    end
  end

  class Index < Model
    private

    alias collection model

    def pagination
      view_context.will_paginate collection, class: 'digg_pagination'
    end
  end
end